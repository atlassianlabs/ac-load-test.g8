import sbt._
import sbt.Keys._

object $name;format="Camel"$Build extends Build {

  lazy val $name;format="camel"$ = Project(
    id = "$name;format="norm"$",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      resolvers += "Gatling repo" at "http://repository.excilys.com/content/groups/public",
      resolvers += "Atlassian's Maven Public Repository" at "https://maven.atlassian.com/content/groups/public",
      resolvers += "Local Maven Repository" at "file://" + Path.userHome + "/.m2/repository",
      name := "$name$",
      organization := "$organization$",
      version := "$version$",
      scalaVersion := "$scala_version$",
      libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.0.0-M3a",
      libraryDependencies += "io.gatling" % "gatling-app" % "2.0.0-M3a",
      libraryDependencies += "com.atlassian.gatling" %% "gatling-jwt" % "0.1"))
}
