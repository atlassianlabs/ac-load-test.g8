import scala.tools.nsc.io.File
import scala.tools.nsc.io.Path.string2path

object PathHelper {

	val gatlingConfUrl = getClass.getClassLoader.getResource("gatling.conf").getPath
	val projectRootDir = File(gatlingConfUrl).parents(3)

	val mavenSourcesDirectory = projectRootDir / "src" / "main" / "scala"
	val mavenResourcesDirectory = projectRootDir / "src" / "main" / "resources"
	val mavenTargetDirectory = projectRootDir / "target" / "scala-2.10"
	val mavenBinariesDirectory = mavenTargetDirectory / "classes"

	val dataDirectory = mavenResourcesDirectory / "data"
	val requestBodiesDirectory = mavenResourcesDirectory / "request-bodies"

	val recorderOutputDirectory = mavenSourcesDirectory
	val resultsDirectory = mavenTargetDirectory / "results"

	val recorderConfigFile = (mavenResourcesDirectory / "recorder.conf").toFile
}