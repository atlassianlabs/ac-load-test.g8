package simulation

import simulation.Config._

object Feeders {

  /**
   * Generates tenant keys like 'tenant-1', 'tenant-2'... and stores the value in the session as "${tenant}"
   */
  val tenantKeyFeeder = (1 to NUM_TENANTS map { n => Map("tenant" -> ("tenant-" + n)) }) toArray

  /**
   * Generates user keys like 'user-1', 'user-2'... and stores the value in the session as "${user}"
   */
  val userKeyFeeder = (1 to NUM_USERS_PER_TENANT map { n => Map("user" -> ("user-" + n)) }) toArray

  /**
   * Generates issue keys like 'ISSUE-1', 'ISSUE-2'... and stores the value in the session as "${issue}"
   */
  val issueKeyFeeder = (1 to NUM_ISSUES_PER_TENANT map { n => Map("issue" -> ("ISSUE-" + n)) }) toArray

}