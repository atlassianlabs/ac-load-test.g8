package simulation

import scala.concurrent.duration.DurationInt
import com.atlassian.gatling.jwt.Predef.httpWithJwt

import io.gatling.core.Predef._
import io.gatling.core.Predef._
import io.gatling.core.Predef.assertions.global
import io.gatling.core.Predef.bootstrap.{exec, feed}
import io.gatling.http.Predef._
import simulation.Config._
import simulation.Feeders._

/**
 * An example simulation against a Connect app.
 *
 * IMPORTANT: run the TenantRegistrationSimulation at least once first, so that the mock tenants that will be used during the load test are registered against your tenant.
 */
class AtlassianConnectSimulation extends Simulation {

  val httpProtocol = http.baseURL(Config.CONNECT_APP)

  val scn = scenario("A mixture of iframe and XHR requests")
    .during(10 seconds) {
      exec(
        // Prepare the session with a tenant key, user key and issue key picked at random.
        feed(tenantKeyFeeder.random)
          .feed(userKeyFeeder.random)
          .feed(issueKeyFeeder.random)
          .exec(
            // Issue a JWT-signed request to the connect app.
            httpWithJwt("iframe-request")
              .get("/connect-app-iframe")
              .queryParam("issue_key", "${issue}")
              .queryParam("user_key", "${user}")
              .queryParam("user_id", "${user}")
              .queryParam("tz", "Australia/Sydney")
              .queryParam("loc", "en-US")
              .sign("${tenant}", "${user}", "mock-tenant-shared-secret")
              .check(status.is(200),
                // Collect the page-view token so we can simulate xhr requests.
                css("meta[name=acpt]", "content").saveAs("acpt-token")))
          .repeat(5) {
            exec(
              // Simulate XHR requests using the page view token
              http("token-based-xhr-request")
                .put("/connect-app-rest-resource")
                .header("X-acpt", "${acpt-token}")
                .check(status.is(200), header("X-Acpt").saveAs("acpt-token")))
          })
    }

  setUp(scn.inject(ramp(TOTAL_USERS users) over (10 seconds)))
    .protocols(httpProtocol)
    .assertions(global.successfulRequests.percent.is(100))
}
