package simulation

object Config {

  val CONNECT_APP = "$connect_app_under_test$"
  val NUM_TENANTS = 40
  val NUM_ISSUES_PER_TENANT = 2
  val NUM_USERS_PER_TENANT = 20
  val TOTAL_USERS = NUM_TENANTS * NUM_USERS_PER_TENANT

}