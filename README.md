# Giter8 Template for Atlassian Connect Load Tests


The template generates an SBT project based on the [Gatling Maven archetype](https://github.com/excilys/gatling/wiki/Ide-integration#wiki-archetype) set up to run [Gatling 2](http://gatling-tool.org/) load tests. 

It includes support for generating JWT-signed requests using [gatling-jwt](https://bitbucket.org/atlassianlabs/gatling-jwt), as well as a couple of example Gatling scenarios showing how you can populate your Connect add-on with mock tenants supplied by [ac-mock-tenants](https://ac-mock-tenants.herokuapp.com), and how you can issue JWT-signed requests, as well as page-token authenticated requests to your connect app on behalf of mock tenants.

## Usage

### Creating a project

1. [Install giter8](https://github.com/n8han/giter8#installation)
2. Create a new load testing project: `g8 https://bitbucket.org/atlassianlabs/ac-load-test.g8.git`
3. Follow the prompts to specify the project name, app under test etc... You can change all these values later if you need to.

If step 2 fails, you might have an old version of giter8 (earlier than 0.6.4). Upgrade, or try the following workaround: `git clone https://bitbucket.org/atlassianlabs/ac-load-test.g8` && `g8 file://ac-load-test.g8`

### Defining your load test

Examine the example scenarios under `src/main/scala/simulation` and adapt them to your needs. For inspiration, see:

* [Gatling documentation](https://github.com/excilys/gatling/wiki/First-Steps-with-Gatling)
* [Gatling cheat sheet](http://gatling-tool.org/cheat-sheet/)
* [Example stress test for Who's Looking based on this template](https://bitbucket.org/atlassianlabs/whoslooking-load-test)

### Running your load test

You can start Gatling with `sbt run`, and select your scenario. 

You'll need to run the `TenantRegistrationSimulation` at least once first, so that your add-on is populated with mock tenants.

You can also run the tests from your IDE or without sbt just by launching the `Engine` main class in the default package.
